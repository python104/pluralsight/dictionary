#dict maps keys to values
#funamental data structure in python
#also knows as maps or associative arrays

d = {"ahsan" : "0345-8819484", 
"Waqar" : "0333-6194388"}   #created in key value pairs,pairs separated by comma, 
                            # and each key is separated from the corresponadance value by a colon
d["Waqar"] = "0333-0000000" #updating the value of key "waqar" using square brakcet

#remember! we initialize dict with curly brackets {} but update the value by square bracket[]
d["EF"] = "Model town link road"
#if we assign a value to a key that is not been added, a new entry is created.

print(d["EF"]) # print individual value of key

print(d)